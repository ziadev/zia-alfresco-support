# Zia Consulting - Alfresco Support Utilities 

This project contains scripts and other utilities to monitor and troubleshoot Alfresco.

## thread-dumps/

Contains scripts and utilities to produce, extract and format thread dumps, so they can be analyzed with other tools, such as the [Spotify's Online Java Thread Dump Analyzer](https://spotify.github.io/threaddump-analyzer/)

* [extract-dump.awk](https://bitbucket.org/ziadev/zia-alfresco-support/src/master/thread-dumps/extract-dump.awk) will extract the thread dumps from a log file.
